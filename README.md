# AWS CLI and K8S profiles Wizard

## Wizard to install and configure awscli v2 with its SSO profiles and its kubeconfig

*  **You need to have Python3 and pip3 installed**
*  **The wizard is based on the following [document](https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2.html) to install the awscli v2.**
*  **The wizard creates the $HOME/.aws/credentials and $HOME/.aws/config files with your SSO user settings..**
*  **The wizard discovers which profiles have access to an EKS cluster and downloads their corresponding kubeconfig.**

## Instructions for use::
  
### 1. Clonar este repositorio en el tag de la versión deseada y posicionarse en el directorio awscli-wizard.
`git clone --depth 1 --branch v1.0.0 git@YOUR_REPO/awscli-wizard.git`

`cd awscli-wizard`

### 2. Install the necessary libraries for the wizard.
`pip3 install -r requirements.txt`

### 3. Run the wizard with Python3.
`unset AWS_ACCESS_KEY_ID`   (To ensure that other credentials do not interfere)

`python3 wizard.py`

### 4. Follow the wizard according to the needs of your deployment.
**Clarifications:**
* Upon startup, the wizard will remove the existing $HOME/.aws/config and $HOME/.aws/credentials files
* The wizard does 2 SSO logins to federate with SAML. One opens the browser automatically and the next one prints the link in the console.
* Kubeconfig are downloaded to $HOME/.kube/${cluster_name}


### 5. Post execution and use of profiles:
*  You can modify the name of the profiles (the one that appears between "[ ]"), which are NOT eks users, created in the file $HOME/.aws/credentials for a more friendly one. 
*  The wizard creates the profiles, but the credentials expire in less than 24 hours, to renew them you must log in again: `aws sso login --profile sso`
*  An AWS profile can be used by exporting its name to the environment variable AWS_PROFILE: `export AWS_PROFILE=nombrePerfil` (linux y mac) o `$env:AWS_PROFILE = “nombrePerfil”` (Windows Powershell)
*  To authenticate in k8s you must export the kubeconfig location to the KUBECONFIG environment variable: `export KUBECONFIG=$HOME/.kube/${cluster_name}` (linux y mac) o `$env:KUBECONFIG = “$HOME/.kube/${cluster_name}”` (Windows Powershell)