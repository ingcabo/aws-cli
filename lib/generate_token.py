import boto3

def generateToken():                            ## ACCESS TOKEN GENERATION

    session = boto3.Session(profile_name='sso')
    client = session.client('sso-oidc')
    registerClient = client.register_client(clientName='script',
                                            clientType='public')
    
    clientId = registerClient['clientId']

    clientSecret = registerClient['clientSecret']

    startDeviceAuthorization = client.start_device_authorization(clientId=clientId,
                                                                 clientSecret=clientSecret,
                                                                 startUrl='https://COMPANY.awsapps.com/start')

    deviceCode = startDeviceAuthorization['deviceCode']

    verificationUriComplete = startDeviceAuthorization ['verificationUriComplete']

    print("Generating Authorization Link..." + '\n' + '\n')
    print("Please enter the following link in your browser and click the button \"Sign in to AWS CLI \":" + '\n')
    print("         ", verificationUriComplete)
    print("")
    print("Don't forget to be logged into your COMPANY mail")

    respuesta = ""
    while (respuesta != "next") and (respuesta != "cancel") :
        print(" ")
        respuesta = input('Once logged in, type \"next\" to continue or \"cancel\" to interrupt the configuration: ')
        print(" ")

        if respuesta == "next":
            try:
                createToken = client.create_token(clientId=clientId,
                                      clientSecret=clientSecret,
                                      grantType='urn:ietf:params:oauth:grant-type:device_code',
                                      deviceCode=deviceCode)
            except:
                print("The process will not be able to continue if you do not enter the generated link and complete the login" + '\n' + '\n' + verificationUriComplete)
                respuesta=""

        elif respuesta == "cancel":
            exit()


    accessToken =  createToken['accessToken']

    return accessToken 