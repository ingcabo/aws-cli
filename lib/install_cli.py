import os
import subprocess
import sys
from subprocess import Popen, PIPE, STDOUT

def installCli(sysOs, which):   #####################################  CLI INSTALLATION

    if (sysOs == "linux"):

        os.system('sudo rm -rf /usr/local/aws-cli')
        os.system('curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"') 
        os.system('unzip -q awscliv2.zip')
        cmd="sudo ./aws/install --bin-dir " + which
        os.system(cmd)
        os.system('sudo rm -rf ./aws/')
        os.system('sudo rm awscliv2.zip')

    elif (sysOs == "darwin"):

        os.system('sudo rm -rf /usr/local/aws-cli')
        os.system('curl "https://awscli.amazonaws.com/AWSCLIV2.pkg" -o "AWSCLIV2.pkg"')
        os.system('sudo installer -pkg AWSCLIV2.pkg -target /')
        os.system('sudo rm AWSCLIV2.pkg')

    elif (sysOs == "win32"):

        os.system('bitsadmin /create downloadawscliv2')
        os.system('bitsadmin /transfer downloadawscliv2 https://awscli.amazonaws.com/AWSCLIV2.msi C:\\Users\\AWSCLIV2.msi')
        os.system('msiexec /i C:\\Users\\AWSCLIV2.MSI /q')
        os.system('setx path "%path%;C:\\Program Files\\Amazon\\AWSCLIV2"')
        os.remove('C:\\Users\\AWSCLIV2.MSI')


def removeCli(sysOs, which):   ##################################### CLI REMOVAL

    if (sysOs == "linux"):
        try:
            cmd="python3 -m pip uninstall awscli -y"
            os.system(cmd)
        except:
            cmd="python -m pip uninstall awscli -y"
            os.system(cmd)              
        cmd=("sudo rm " + which + "/aws" )
        os.system(cmd)
        cmd=("sudo rm " + which + "/aws_completer" )
        os.system(cmd)
        os.system('sudo rm -rf /usr/local/aws-cli')

    elif (sysOs == "darwin"):

        cmd= pathPy + " -m pip uninstall awscli -y"
        os.system(cmd)  
        cmd=("sudo rm " + which + "/aws" )
        os.system(cmd)
        cmd=("sudo rm " + which + "/aws_completer" )
        os.system(cmd)
        os.system('sudo rm -rf /usr/local/aws-cli')

    elif (sysOs == "win32"):

        os.system('wmic product where name="AWS Command Line Interface" call uninstall /nointeractive')
        os.system('wmic product where name="AWS Command Line Interface v2" call uninstall /nointeractive')


def upgradeCli(sysOs, which):   ##################################### CLI UPDATE

    if (sysOs == "linux"):

        os.system('sudo rm -rf /usr/local/aws-cli')
        os.system('curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"') 
        os.system('unzip -q awscliv2.zip')
        
        cmd="sudo ./aws/install --bin-dir " + which + " --update"
        os.system(cmd)
        os.system('sudo rm -rf ./aws/')
        os.system('sudo rm awscliv2.zip')

    elif (sysOs == "darwin"):

        os.system('sudo rm -rf /usr/local/aws-cli')

        os.system('curl "https://awscli.amazonaws.com/AWSCLIV2.pkg" -o "AWSCLIV2.pkg"')
        os.system('sudo installer -pkg AWSCLIV2.pkg -target /')
        os.system('sudo rm AWSCLIV2.pkg')

    elif (sysOs == "win32"):

        os.system('bitsadmin /create downloadawscliv2')
        os.system('bitsadmin /transfer downloadawscliv2 https://awscli.amazonaws.com/AWSCLIV2.msi C:\\Users\\AWSCLIV2.msi')
        os.system('msiexec /i C:\\Users\\AWSCLIV2.MSI /q')
        os.system('setx path "%path%;C:\\Program Files\\Amazon\\AWSCLIV2"')
        os.remove('C:\\Users\\AWSCLIV2.MSI')  



def configUnixCli():               ####################### I configure SSO profiles in the CLI for MAC and Linux ###############

    home=subprocess.check_output("echo $HOME", shell=True).decode()
    home=home.replace('\n', '').replace('\r', '')                                

##### I start configuring the profile to authenticate in SSO

    response=""
    while (response != "next") and (response != "cancel") :
        print(" ")
        response = input('WARNING, this script will remove ' + home + '/.aws/credentials y ' + home + '/.aws/config ... type \"next\" to continue or \"cancel\" to interrupt the configuration : ')
        print(" ")

        if response == "cancel":
            exit()

    awsDir=home + "/.aws/"
    kubeDir=home + "/.kube/"                                                        

    if os.path.isdir(awsDir):
        cmd='sudo rm ' + awsDir + "credentials"
        os.system(cmd)
        cmd='sudo rm ' + awsDir + "config"
        os.system(cmd)   

    else:
        cmd="mkdir " + awsDir                                                     
        os.system(cmd)

    cmd="touch " + awsDir + "credentials " + awsDir + "config"           
    os.system(cmd)


    response=""
    #### this means that administrator or security roles are in a separate case
    while (response != "sre") and (response != "security") and (response != "other") and (response != "cancel"):
        print(" ")
        response = input('which team does he belong to? respond: \"sre\" \"security\" \"other\" or \"cancel\" to interrupt the configuration : ')
        print(" ")

    if response == "cancel":
        exit() 

    elif response == "sre":

        accountId="xxxxxx062"
        roleName="CROSS-Admin-Policy"

    elif response == "security":

        accountId="xxxxxxx478"
        roleName="CROSS-Admin-Policy" 

    elif response == "other":

        accountId="xxxxxx478"
        roleName="CROSS-Sbx-Policy" 

    ssoProfile= """
[sso]
sso_start_url = https://COMPANY.awsapps.com/start
sso_region = us-east-1
sso_account_id = """ + accountId + """
sso_role_name = """ + roleName + """
region = us-east-1
output = json

"""
    cmd="echo \"" + ssoProfile + "\" > " + awsDir + "credentials" 
    os.system(cmd)

    awsConfig= """
[default]
region = us-east-1
output = json

"""
    cmd="echo \"" + awsConfig + "\" > " + awsDir + "config" 
    os.system(cmd)

    return home, awsDir, kubeDir


def configWinCli():               ####################### I configure the SSO profiles in the CLI FOR WINDOWS ###############

    home=subprocess.check_output("powershell echo $HOME", shell=True).decode()
    home=home.replace('\n', '').replace('\r', '')                                

##### I start configuring the profile to authenticate in SSO

    response=""
    while (response != "next") and (response != "cancel") :
        print(" ")
        response = input('WARNING, this script will remove $HOME\\.aws\\credentials y $HOME\\.aws\\config ... type \"next\" to continue or \"cancel\" to interrupt the configuration : ')
        print(" ")

        if response == "cancel":
            exit()

    awsDir=home + "\\.aws\\"
    kubeDir=home + "\\.kube\\"                                                        

    if os.path.isdir(awsDir):
        pass

    else:
        cmd="MD " + awsDir                                                     
        os.system(cmd)

    cmd="echo   >" + awsDir + "credentials "
    os.system(cmd)

    cmd="echo   >" + awsDir + "config"
    os.system(cmd)

    cmd=awsDir + "credentials"
    os.remove(cmd)
    cmd=awsDir + "config"
    os.remove(cmd)

    response=""
    while (response != "sre") and (response != "security") and (response != "other") and (response != "cancel"):
        print(" ")
        response = input('which team does he belong to ? respond: \"sre\" \"security\" \"other\ or \"cancel\" to interrupt the configuration : ')
        print(" ")

    if response == "cancel":
        exit() 

    elif response == "sre":

        accountId="XXXXXXXX062"
        roleName="CROSS-Admin-Policy"

    elif response == "security":

        accountId="XXXXXX478"
        roleName="CROSS-Admin-Policy"

    elif response == "other":

        accountId="XXXXXXX478"
        roleName="CROSS-Sbx-Policy"

    cmd= "echo [sso] >> " + awsDir + "credentials"
    os.system(cmd)
    cmd=" echo sso_start_url = https://COMPANY.awsapps.com/start >> " + awsDir + "credentials"
    os.system(cmd)
    cmd="echo sso_region = us-east-1 >> " + awsDir + "credentials"
    os.system(cmd)
    cmd="echo sso_account_id = " + accountId + " >> " + awsDir + "credentials" 
    os.system(cmd)
    cmd="echo sso_role_name = " + roleName + " >> " + awsDir + "credentials" 
    os.system(cmd)
    cmd="echo region = us-east-1 >> " + awsDir + "credentials"
    os.system(cmd)
    cmd="echo output = json >> " + awsDir + "credentials"
    os.system(cmd)
    cmd="echo; >> " + awsDir + "credentials"
    os.system(cmd)


    cmd= "echo [default] >> " + awsDir + "config"
    os.system(cmd)
    cmd="echo region = us-east-1 >> " + awsDir + "config"
    os.system(cmd)
    cmd="echo output = json >> " + awsDir + "config"
    os.system(cmd)
    cmd="echo; >> " + awsDir + "config"
    os.system(cmd)

    return home, awsDir, kubeDir


