import boto3
import os
import lib.generate_token as generate_token
import subprocess
import re
from subprocess import Popen, PIPE, STDOUT


def cheks_k8s_access(home, cluster_name, profile):

    cmd='aws eks update-kubeconfig --region us-east-1 --name ' + cluster_name + ' --kubeconfig '+ home + '/.kube/temporal --profile ' + profile
    os.system(cmd)
    
    os.environ['KUBECONFIG']=home+"/.kube/temporal"
    
    cmd='kubectl get pods -n false_namespace'
    p = Popen(cmd, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    output = p.stdout.readline().decode()
    output = output.replace('\n', '').replace('\r', '') 

    try:
        regex=re.search(r'\((.*?)\)',output).group(1)

        if regex == "Forbidden":
            eks_user=True

        elif regex == "Unauthorized":
            eks_user=False
    except:

        if output == "No resources found in false_namespace namespace.":
            eks_user=True

        else:
            eks_user=False

    cmd='sudo rm ' + home + '/.kube/temporal'
    os.system(cmd)

    return eks_user



def configProfiles(home, awsDir, kubeDir):
    
## Me autentico en el SSO para empezar a configurar los demas profiles

    print(" ")
    print("Authenticating the AWS SSO of EQUINOX media...") 
    print(" ")
    os.system('aws sso login --profile sso')
    print(" ")
    print("successful authentication")
    print(" ")

    generatedToken=generate_token.generateToken()

    print(" ")
    print(" ")
    print(" preparing profiles ...")
    print(" ")
    print(" ")

    session = boto3.Session(profile_name='sso')

    client = session.client('sso')

    response = client.list_accounts(maxResults=123, 
                                    accessToken=generatedToken)

    for profile in response['accountList']:

        awsId=profile['accountId']
        awsName=profile['accountName'].lower()

        account_roles = client.list_account_roles(maxResults=123,
                                             accessToken=generatedToken,
                                             accountId=awsId)
    
        for roles in account_roles['roleList']:
            awsRole=roles['roleName']

            awsProfile = awsName + "/" + awsRole.lower()

            cmd= "echo [" + awsProfile + "] >> " + awsDir + "credentials"
            os.system(cmd)
            cmd=" echo sso_start_url = https://COMPANY.awsapps.com/start >> " + awsDir + "credentials"
            os.system(cmd)
            cmd="echo sso_region = us-east-1 >> " + awsDir + "credentials"
            os.system(cmd)
            cmd="echo sso_account_id = " + awsId + " >> " + awsDir + "credentials" 
            os.system(cmd)
            cmd="echo sso_role_name = " + awsRole + " >> " + awsDir + "credentials" 
            os.system(cmd)
            cmd="echo region = us-east-1 >> " + awsDir + "credentials"
            os.system(cmd)
            cmd="echo output = json >> " + awsDir + "credentials"
            os.system(cmd)
                
            

## Check if the role has access to the cluster

            eksSession = boto3.Session(profile_name=awsProfile)
            eks = eksSession.client('eks')

            try:
                clusters = eks.list_clusters(maxResults=100)['clusters']

            except:
                clusters = []        
                eks_user=False 

            for cluster in clusters:
            
                cluster_name = eks.describe_cluster(name=cluster)['cluster']['name']
                eks_user=cheks_k8s_access(home, cluster_name, awsProfile)

                if eks_user:
            
                    cmd="aws eks update-kubeconfig --region us-east-1 --name " + cluster_name + " --kubeconfig " + kubeDir + cluster_name + " --profile " + awsProfile   
                    cmd=cmd.replace('\n', '').replace('\r', '')
                    os.system(cmd)

