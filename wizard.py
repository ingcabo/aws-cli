import os
import subprocess
import sys
from subprocess import Popen, PIPE, STDOUT
import lib.install_cli as cli 
import lib.profiles as profiles 

sysOs=sys.platform          ##Detecto OS

checkAws = os.system('aws --version')

which=os.system('which aws')

if which == 0:
    which=subprocess.check_output("which aws", shell=True).decode()
    which=which.replace('\n', '').replace('\r', '')
    which=which.lower().split('/aws')[0]

else:
    which="/usr/local/bin"

if checkAws == 0:

    versionAws = subprocess.check_output("aws --version", shell=True).decode()
    checkVersion= int(versionAws[8:9])

    if checkVersion < 2:

        print(" ")
        print("A version of AWS CLI v1 installed has been detected...")
        print(" ")
        print("Your current version is:")
        print(" ")
        print(versionAws)
        print(" ")
        print("AWS CLI v2 update begins...")
        upgradeCli(sysOs, which)
        print(" ")
        versionAws = subprocess.check_output("aws --version", shell=True).decode()
        checkVersion= int(versionAws[8:9])

        if checkVersion < 2:

            cli.removeCli(sysOs, pathPy, which)
            print(" ")
            cli.installCli(sysOs, which)
            print(" ")
            print("The installation was successful, your current version is: ")
            print(" ")
            versionAws = subprocess.check_output("aws --version", shell=True).decode()
            print(versionAws)

        else:

            print("The installation was successful, your current version is: ")
            print(" ")
            versionAws = subprocess.check_output("aws --version", shell=True).decode()
            print(versionAws)


    else:

        print(" ")
        print("The version of the AWS CLI installed does not require an update...")
        print(" ")
        print("Your current version is:")
        print(" ")
        print(versionAws)

    
else:
    
    print(" ")
    print("The AWS CLI v2 installation begins...")
    print(" ")
    cli.installCli(sysOs, which)
    versionAws = subprocess.check_output("aws --version", shell=True).decode()
    print(" ")
    print("The installation was successful, your current version is: ")
    print(" ")
    print(versionAws)



################################ Comienzo a configurar los perfiles de AWS SSO  #########################

if sysOs == "darwin" or sysOs == "linux":

    config=cli.configUnixCli()
    home=config[0]
    awsDir=config[1]
    kubeDir=config[2]

    profiles.configProfiles(home, awsDir, kubeDir)

else:

    config=cli.configWinCli()
    home=config[0]
    awsDir=config[1]
    kubeDir=config[2]

    profiles.configProfiles(home, awsDir, kubeDir)
    
print(" the end...")

